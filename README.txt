flag_hidden allows site moderators to flag a nodes and comments as needing to
be hidden. It works similarly to the D6 hidden module, which explains more:

    "Open Publishing" sites require transparency of the "open editing"
    process.

   This module creates a way of removing nodes and comments from the main
   site, but keeping them accessible to users in a special location with
   an explanation of the editorial reasons for "hiding".

flag_hidden supports the following features:

* Adds a noindex, nofollow META tag when viewing hidden nodes.
* Allows users to show hidden comments for a node they are viewing.
* Uses the flag module to let site admins perform arbitrary
  actions for hidden articles/comments. It also

flag_hidden also comes with a companion module called flag_hidden_tags, which
uses a taxonomy to identify the reasons an article is hidden and provides a
simple interface for moderators.
