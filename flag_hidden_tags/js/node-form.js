(function ($) {

Drupal.behaviors.flagHiddenTagsFieldset = {
  attach: function (context) {
    $('#edit-hidden', context)
      .drupalSetSummary(function (context) {
        var checkboxes = $('input.form-checkbox', context);
        var textfields = $('input.form-text', context);
        if (checkboxes.is(':checked') || textfields.val()) {
          return Drupal.t('Hidden');
        } else {
          return Drupal.t('Not hidden');
        }
    });
  }
};

})(jQuery);

